package ec;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;

/**
 * 購入履歴画面
 * @author d-yamaguchi
 *
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション開始
				HttpSession session = request.getSession();
				try {

					String id = request.getParameter("id");
					int idi = Integer.parseInt(id);

					BuyDAO BDA = new BuyDAO();
					BuyDataBeans buy = BDA.tuika2(idi);
					request.setAttribute("buy", buy);

					BuyDAO BDA3 = new BuyDAO();
					List<ItemDataBeans> buyList3 = BDA3.tuika3(idi);
					request.setAttribute("buyList3", buyList3);

					request.getRequestDispatcher(EcHelper.USER_BUY_HISTORY_DETAIL_PAGE).forward(request, response);

				} catch (Exception e) {
					e.printStackTrace();
					session.setAttribute("errorMessage", e.toString());
					response.sendRedirect("Error");
				}
			}

		}
