package ec;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;

@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher(EcHelper.LOGIN_PAGE).forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

	try {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");


		 UserDAO userDao = new UserDAO();
		  int user = userDao.findByLoginInfo(loginId, password);

		  if(user==0) {
			  request.setAttribute("errMsg", "ログインに失敗しました。");

			  RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ec/login.jsp");
			  dispatcher.forward(request, response);
			  return;

		  }

		  session.setAttribute("userId", user);
		  session.setAttribute("isLogin", true);

		  response.sendRedirect("Index");
		   } catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}

	 }
}
