package dao;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class UserDAO {
	// インスタンスオブジェクトを返却させてコードの簡略化

	/**
	 * データの挿入処理を行う。現在時刻は挿入直前に生成
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_user(name,login_id,address,login_password,create_date,update_date) VALUES(?,?,?,?,?,?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getLoginId());
			st.setString(3, udb.getAddress());
			String result = passpass(udb.getPassword());
			st.setString(4, result);
			st.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */

	/**
	 * ユーザーIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */
	public static UserDataBeans getUserDataBeansByUserId(int userId) throws SQLException {
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT id,name, login_id, address FROM t_user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setAddress(rs.getString("address"));
			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return udb;
	}

	/**
	 * ユーザー情報の更新処理を行う。
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void updateUser(UserDataBeans udb) throws SQLException {
		// 更新された情報をセットされたJavaBeansのリスト
		UserDataBeans updatedUdb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET name=?, login_id=?, address=? WHERE id=?;");
			st.setString(1, udb.getName());
			st.setString(2, udb.getLoginId());
			st.setString(3, udb.getAddress());
			st.setInt(4, udb.getId());
			st.executeUpdate();
			System.out.println("update has been completed");

			st = con.prepareStatement("SELECT name, login_id, address FROM t_user WHERE id=" + udb.getId());
			ResultSet rs = st.executeQuery();

			while (rs.next()) {

				updatedUdb.setName(rs.getString("name"));
				updatedUdb.setLoginId(rs.getString("login_id"));
				updatedUdb.setAddress(rs.getString("address"));
			}

			st.close();
			System.out.println("searching updated-UserDataBeans has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * loginIdの重複チェック
	 *
	 * @param loginId
	 *            check対象のログインID
	 * @param userId
	 *            check対象から除外するuserID
	 * @return bool 重複している
	 * @throws SQLException
	 */
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

////////UM//////////////////////////////////////////////////////////////////////////////

	public List<UserDataBeans> findAll() {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM t_user where login_id not in ('admin')";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String loginId2 = rs.getString("login_id");
				String password = rs.getString("login_password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				UserDataBeans user = new UserDataBeans(id, name,address,loginId2,password, createDate, updateDate);

				userList.add(user);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;
	}


public UserDataBeans findBySentakuInfo(int id) {
	Connection conn = null;
	UserDataBeans user = null;

	try {
		conn = DBManager.getConnection();

		String sql = "SELECT * FROM t_user where id=?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, id);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		int id2 = rs.getInt("id");
		String loginId = rs.getString("login_id");
		String name = rs.getString("name");
		String password = rs.getString("login_password");
		String address = rs.getString("address");
		String createDate = rs.getString("create_date");
		String updateDate = rs.getString("update_date");
		user = new UserDataBeans(id2, name,address,loginId,password, createDate, updateDate);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			}
		}
	}
	return user;
}

public UserDataBeans findBykakuninInfo(String loginId) {
	Connection conn = null;
	PreparedStatement stmt = null;

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// TODO ここに処理を書いていく
		String sql = "select*from t_user where login_id=?";
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, loginId);

		ResultSet rs = stmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		String loginIdData = rs.getString("login_id");

		return new UserDataBeans(loginIdData);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}

public UserDataBeans findByDeleteInfo(String loginId) {
	Connection conn = null;
	PreparedStatement stmt = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// TODO ここに処理を書いていく
		String sql = "delete from t_user where login_id=?";
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, loginId);

		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	return null;
}

public List<UserDataBeans> findByWord(String loginId, String name, String address) {
	Connection conn = null;
	List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

	try {
		conn = DBManager.getConnection();

		String sql = "SELECT * FROM t_user where login_id not in ('admin')";

		if(!(loginId.equals(""))) {
			sql += " AND login_id = '" + loginId +"'";
		}

		if(!(name.equals(""))) {
			sql += " AND name like '%" + name +"%'";
		}

		if(!(address.equals(""))) {
			sql += " AND address like '%" + address +"%'";
		}


		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			int id = rs.getInt("id");
			String loginId1 = rs.getString("login_id");
			String name1 = rs.getString("name");
			String address2 = rs.getString("address");
			String password = rs.getString("login_password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			UserDataBeans user = new UserDataBeans(id, name1, address2,loginId1, password, createDate, updateDate);

			userList.add(user);

		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			}
		}
	}
	return userList;
}

public UserDataBeans findByUpdateInfo(int id, String password, String name, String address, String updateDate) {
	Connection conn = null;
	UserDataBeans user = null;

	try {
		conn = DBManager.getConnection();

		String sql = "UPDATE t_user set name=?,address=?,update_date=? password=? where id=?";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, name);
		stmt.setString(2, address);
		String result = passpass(password);
		stmt.setString(3, result);
		stmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
		stmt.setInt(5, id);
		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return user;
}

public UserDataBeans findByUpdateInfo2(int id, String userName, String address, String updateDate) {

	Connection conn = null;
	UserDataBeans user = null;

	try {
		conn = DBManager.getConnection();

		String sql = "UPDATE t_user set name=?,address=?,update_date=? where id=?";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, userName);
		stmt.setString(2, address);
		stmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
		stmt.setInt(4, id);

		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			}
		}
	}
	return user;
}
public UserDataBeans InsertIntoCreateInfo(String name, String loginId, String address, String password) {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();
		st = con.prepareStatement("INSERT INTO t_user(name,login_id,address,login_password,create_date,update_date) VALUES(?,?,?,?,?,?)");
		st.setString(1, name);
		st.setString(2, loginId);
		st.setString(3, address);
		String result = passpass(password);
		st.setString(4, result);
		st.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
		st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
		st.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return null;
}
public UserDataBeans findByLoginInfo2(String loginId, String password) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// TODO ここに処理を書いていく
		String sql = "select*from t_user where login_id = ? and login_password = ?";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, loginId);
		String result = passpass(password);
		stmt.setString(2, result);
		ResultSet rs = stmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		int iddate = rs.getInt("id");
		String createdate = rs.getString("create_date");
		String updatedate = rs.getString("update_date");
		String loginIdData = rs.getString("login_id");
		String name = rs.getString("name");
		String loginPassword = rs.getString("login_password");
		String addressdata = rs.getString("address");
		return new UserDataBeans(iddate, name,addressdata,loginIdData,loginPassword,createdate,updatedate);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
public static String passpass(String password) {
	String result = null;

		//暗号化MD5
		String source = password;

		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";
		byte[] bytes = null;
		try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		result = DatatypeConverter.printHexBinary(bytes);

	return result;
}
public int findByLoginInfo(String loginId, String password) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ?");
		st.setString(1, loginId);

		ResultSet rs = st.executeQuery();

		int userId = 0;
		while (rs.next()) {
			if (passpass(password).equals(rs.getString("login_password"))) {
				userId = rs.getInt("id");
				System.out.println("login succeeded");
				break;
			}
		}

		System.out.println("searching userId by loginId has been completed");
		return userId;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}
public UserDataBeans findByDetailInfo(int id) {
	Connection conn = null;
	UserDataBeans user = null;

	try {
		conn = DBManager.getConnection();

		String sql = "SELECT * FROM t_user where id=?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, id);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		int id2 = rs.getInt("id");
		String loginId = rs.getString("login_id");
		String name = rs.getString("name");
		String address = rs.getString("address");
		String password = rs.getString("login_password");
		String createDate = rs.getString("create_date");
		String updateDate = rs.getString("update_date");
		user = new UserDataBeans(id2, name, address, loginId, password, createDate, updateDate);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;

			}
		}
	}
	return user;
}

}
