package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {



	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <ItemDataBeans>
	 * @throws SQLException
	 */
	public ArrayList<ItemDataBeans> getItem() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
// ORDER BY RAND() LIMIT ?
			st = con.prepareStatement("SELECT * FROM m_item ");

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setTagId(rs.getInt("tag_id"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<ItemDataBeans> getTag() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
// ORDER BY RAND() LIMIT ?
			st = con.prepareStatement("SELECT * FROM m_item"
												+ " JOIN tag"
												+ " on m_item.tag_id = tag.id");

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList2 = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item2 = new ItemDataBeans();
				item2.setId(rs.getInt("id"));
				item2.setName(rs.getString("name"));
				item2.setDetail(rs.getString("detail"));
				item2.setPrice(rs.getInt("price"));
				item2.setFileName(rs.getString("file_name"));
				item2.setTagId(rs.getInt("tag_id"));
				item2.setTag(rs.getString("tag"));
				itemList2.add(item2);
			}
			System.out.println("getAllItem completed");
			return itemList2;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<ItemDataBeans> getTag2() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
// ORDER BY RAND() LIMIT ?
			st = con.prepareStatement("SELECT * FROM tag");

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList3 = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item3 = new ItemDataBeans();
				item3.setId(rs.getInt("id"));
				item3.setTag(rs.getString("tag"));
				itemList3.add(item3);
			}
			System.out.println("getAllItem completed");
			return itemList3;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	/**
	 * 商品IDによる商品検索
	 * @param itemId
	 * @return ItemDataBeans
	 * @throws SQLException
	 */
	public static ItemDataBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setTagId(rs.getInt("tag_id"));
			}

			System.out.println("searching item by itemID has been completed");

			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品検索
	 * @param searchWord
	 * @param pageNum
	 * @param pageMaxItemCount
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getItemsByItemName(String searchWord, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM m_item ORDER BY id ASC LIMIT ?,? ");
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);
			} else {
				// 商品名検索
				st = con.prepareStatement("SELECT * FROM m_item where name like ?  ORDER BY id ASC LIMIT ?,? ");
				st.setString(1, "%" + searchWord + "%");
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setTagId(rs.getInt("tag_id"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	/**
	 * 商品総数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getItemCount(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from m_item where name like ?");
			st.setString(1, "%" + searchWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	/**
	 * tag検索
	 * @param tagId
	 * @param pageNum
	 * @param pageMaxItemCount
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getTag2(int tagId, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

				// 商品名検索
				st = con.prepareStatement("SELECT * FROM m_item"
													+ " JOIN tag"
													+ " on m_item.tag_id = tag.id"
													+ " where m_item.tag_id = ?"
													+ " ORDER BY m_item.id ASC LIMIT ?,? ");
				st.setInt(1, tagId);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);


			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static double getItemCount2(int tagId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from m_item where tag_id = ?");
			st.setInt(1, tagId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	public ItemDataBeans InsertIntoCreateInfo(String name, String detail, String price, String file_name, String tag_id) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO m_item(name,detail,price,file_name,tag_id) VALUES(?,?,?,?,?)");
			st.setString(1, name);
			st.setString(2, detail);
			st.setString(3, price);
			st.setString(4, file_name);
			st.setString(5, tag_id);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}
	public ItemDataBeans findByDetailInfo(int id) {
		Connection conn = null;
		ItemDataBeans item = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM m_item"
					+ " join tag"
					+ " on m_item.tag_id = tag.id"
					+ " where m_item.id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id2 = rs.getInt("id");
			String name = rs.getString("name");
			String detail = rs.getString("detail");
			int price = rs.getInt("price");
			String file_name = rs.getString("file_name");
			int tag_id = rs.getInt("tag_id");
			String tag = rs.getString("tag");
			item = new ItemDataBeans(id2, name, detail, price, file_name, tag_id,tag);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return item;
	}

	public ItemDataBeans findByDeleteInfo(String id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "delete from m_item where id=?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public ItemDataBeans findByUpdateInfo(int id, String name, String detail, int price, String fileName, int tagId) {
		Connection conn = null;
		ItemDataBeans item = null;

		try {
			conn = DBManager.getConnection();

			String sql = "UPDATE m_item set name=?,detail=?,price=?,file_name=?,tag_id=? where id=?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, detail);
			stmt.setInt(3, price);
			stmt.setString(4, fileName);
			stmt.setInt(5, tagId);
			stmt.setInt(6, id);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return item;
	}

	public List<ItemDataBeans> findByWord(String id, String name, String FP, String TP) {
		Connection conn = null;
		List<ItemDataBeans> sList = new ArrayList<ItemDataBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM m_item WHERE id > 0";

			if(!(id.equals(""))) {
				sql += " AND id = '" + id +"'";
			}

			if(!(name.equals(""))) {
				sql += " AND name like '%" + name +"%'";
			}

			if(!(FP.equals(""))) {
				sql += " AND price >= '" + FP +"'";
			}
			if(!(TP.equals(""))) {
				sql += " AND price <= '" + TP +"'";
			}


			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();

				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setTagId(rs.getInt("tag_id"));
				sList.add(item);

				sList.add(item);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return sList;
	}

		public ItemDataBeans InsertIntoTagCreateInfo( String id, String tag) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO tag(id,tag) VALUES(?,?)");
			st.setString(1, id);
			st.setString(2, tag);
			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}


}
