package beans;
import java.io.Serializable;

public class ItemDataBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String fileName;
	private int tagId;
	private String tag;
	private String saki;
	private String ato;

	public ItemDataBeans(String name2, int price2) {
		this.name = name2;
		this.price = price2;
		}

	public ItemDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public ItemDataBeans(int id2, String name2, String detail2, int price2, String file_name, int tag_id, String tag) {
		this.id = id2;
		this.name = name2;
		this.detail = detail2;
		this.price = price2;
		this.fileName = file_name;
		this.tagId = tag_id;
		this.tag = tag;
	}

	public ItemDataBeans(int id2, String name2, String detail2, int price2, String file_name, int tag_id) {
		this.id = id2;
		this.name = name2;
		this.detail = detail2;
		this.price = price2;
		this.fileName = file_name;
		this.tagId = tag_id;
	}

	public ItemDataBeans(int id3, String name1, String FP, String TP) {
		this.id = id3;
		this.name = name1;
		this.saki = FP;
		this.ato = TP;
	}

	public int getId() {
		return id;
	}
	public void setId(int itemId) {
		this.id = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String itemName) {
		this.name = itemName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int itemPrice) {
		this.price = itemPrice;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String filename) {
		this.fileName = filename;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}


	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getSaki() {
		return saki;
	}

	public void setSaki(String saki) {
		this.saki = saki;
	}

	public String getAto() {
		return ato;
	}

	public void setAto(String ato) {
		this.ato = ato;
	}


}

