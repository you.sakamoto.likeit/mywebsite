package beans;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyDataBeans  implements Serializable {
	private int id;
	private int userId;
	private int totalPrice;
	private int delivertMethodId;
	private Date buyDate;

	private String deliveryMethodName;
	private int deliveryMethodPrice;

	private int buyId;
	private int itemId;
	private String name;
	private int price;

	public BuyDataBeans(int id2, int totalPrice, Date buyDate2, int deliveryMethodId, int userId1,
			int deliveryMethodPrice2, String deliveryMethodName2) {
		this.id = id2;
		this.totalPrice = totalPrice;
		this.buyDate = buyDate2;
		this.delivertMethodId = deliveryMethodId;
		this.userId = userId1;
		this.deliveryMethodPrice = deliveryMethodPrice2;
		this.deliveryMethodName = deliveryMethodName2;

	}

	public BuyDataBeans(int id2, int buyId2, int totalPrice2, Timestamp buyDate2, int deliveryMethodId, int userId1,
			int deliveryMethodPrice2, String deliveryMethodName2) {
		this.id = id2;
		this.buyId = buyId2;
		this.totalPrice = totalPrice2;
		this.buyDate = buyDate2;
		this.delivertMethodId = deliveryMethodId;
		this.userId = userId1;
		this.deliveryMethodPrice = deliveryMethodPrice2;
		this.deliveryMethodName = deliveryMethodName2;
	}

	public BuyDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public BuyDataBeans(int id2, int buyId, int itemId, String name, int price) {
		this.id = id2;
		this.buyId = buyId;
		this.itemId = itemId;
		this.name = name;
		this.price = price;
	}

	public int getBuyId() {
		return buyId;
	}
	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}


	public int getDelivertMethodId() {
		return delivertMethodId;
	}
	public void setDelivertMethodId(int delivertMethodId) {
		this.delivertMethodId = delivertMethodId;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}
	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buyDate);
	}
	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice);
	}
	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}
	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}



}
