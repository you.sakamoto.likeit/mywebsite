package beans;
import java.io.Serializable;

public class UserDataBeans implements Serializable {
	private String name;
	private String address;
	private String loginId;
	private String password;
	private int id;
	private String createDate;
	private String updateDate;

	// コンストラクタ
	public UserDataBeans() {
		this.name = "";
		this.address = "";
		this.loginId = "";
		this.password = "";
		this.createDate = "";
		this.updateDate = "";
	}

	public UserDataBeans(int id, String name, String address, String loginId, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.loginId = loginId;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public UserDataBeans(String loginIdData, String loginPassword, String addressdata) {
		this.loginId = loginIdData;
		this.password = loginPassword;
		this.address = addressdata;
	}


	public UserDataBeans(String loginId2, String name2) {
		this.loginId = loginId2;
		this.name = name2;
	}

	public UserDataBeans(String loginIdData, String loginPassword, String addressdata, int iddate, String createdate2,
			String updatedate2, String name2) {
		this.loginId = loginIdData;
		this.password = loginPassword;
		this.address = addressdata;
		this.id = iddate;
		this.createDate = createdate2;
		this.updateDate = updatedate2;
		this.name = name2;
	}

	public UserDataBeans(String loginIdData) {
		this.loginId = loginIdData;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public void findBySentakuInfo(int id1) {
		this.id = id1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * ユーザー情報更新時の必要情報をまとめてセットするための処理
	 *
	 * @param name
	 * @param loginId
	 * @param address
	 */
	public void setUpdateUserDataBeansInfo(String name, String loginId, String address, int id) {
		this.name = name;
		this.loginId = loginId;
		this.address = address;
		this.id = id;
	}

}
