package UserManagement;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class MUpdate
 */
@WebServlet("/MUpdate")
public class MUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String id = request.getParameter("id");
		int idi = Integer.parseInt(id);

		ItemDAO itemDao = new ItemDAO();
		ItemDataBeans item = itemDao.findByDetailInfo(idi);

		request.setAttribute("item", item);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/um/mUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		int idi = Integer.parseInt(id);
		String sName = request.getParameter("sName");
		String sDetail = request.getParameter("sDetail");

		String sMoney = request.getParameter("sMoney");
		int sMoney2 = Integer.parseInt(sMoney);
		String m = "" + sMoney2;
		m = new Integer(sMoney2).toString();
		m = Integer.toString(sMoney2);

		String fileName = request.getParameter("fileName");

		String sCategory = request.getParameter("sCategory");
		int sCategory2 = Integer.parseInt(sCategory);
		String c = "" + sCategory2;
		c = new Integer(sCategory2).toString();
		c = Integer.toString(sCategory2);

			if(sName.equals("")||sDetail.equals("")||m.equals("")||fileName.equals("")||c.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				  RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/um/mUpdate.jsp");
				  dispatcher.forward(request, response);
				  return;
			}

			ItemDAO userDao2 = new ItemDAO();
		    userDao2.findByUpdateInfo(idi,sName,sDetail,sMoney2,fileName,sCategory2);

		    response.sendRedirect("MList");
	}

}
