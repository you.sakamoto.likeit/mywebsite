package UserManagement;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class Mlist
 */
@WebServlet("/MList")
public class MList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}


		ItemDAO itemDao = new ItemDAO();
		List<ItemDataBeans> itemList;

			itemList = itemDao.getItem();

		request.setAttribute("itemList", itemList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/um/mList.jsp");
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("sId");
//		int id2 = Integer.parseInt(id);
//		String i = "" + id2;
//		i = new Integer(id2).toString();
//		i = Integer.toString(id2);

		String name = request.getParameter("sName");
		String saki = request.getParameter("saki");
//		int saki2 = Integer.parseInt(saki);
//		String s = "" + saki2;
//		s = new Integer(saki2).toString();
//		s = Integer.toString(saki2);

		String ato = request.getParameter("ato");
//		int ato2 = Integer.parseInt(ato);
//		String a = "" + ato2;
//		a = new Integer(ato2).toString();
//		a = Integer.toString(ato2);

		ItemDAO itemDao = new ItemDAO();
		List<ItemDataBeans> itemList = itemDao.findByWord(id, name, saki, ato);

		request.setAttribute("itemList", itemList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/um/mList.jsp");
		dispatcher.forward(request, response);

	}
}
