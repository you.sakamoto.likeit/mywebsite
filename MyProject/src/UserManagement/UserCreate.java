package UserManagement;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserCreate
 */
@WebServlet("/UserCreate")
public class UserCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/um/UserCreate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * 入力内容を確認 バリデーションエラーがある場合新規登録画面に遷移
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");

			String userName = request.getParameter("userName");
			String address = request.getParameter("address");
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");
			String passwordConf = request.getParameter("passwordConf");

			UserDAO userDao2 = new UserDAO();
			  UserDataBeans user = userDao2.findBykakuninInfo(loginId);

			if(loginId.equals("")||password.equals("")||passwordConf.equals("")||userName.equals("")||address.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				  RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
				  dispatcher.forward(request, response);
				  return;

			}else if(!password.equals(passwordConf)) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
				  dispatcher.forward(request, response);
				  return;

			}else if(!(user==null)) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
				  dispatcher.forward(request, response);
				  return;
			}


			 UserDAO userDao = new UserDAO();
			 userDao.InsertIntoCreateInfo(userName,loginId,address, password);

			  response.sendRedirect("UserListServlet");
		}
	}
