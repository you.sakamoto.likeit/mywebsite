package UserManagement;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

//		HttpSession session = request.getSession();
//		if(session.getAttribute("userId") != null) {
//			response.sendRedirect("UserListServlet");
//			return;}

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/um/Login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

	try {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		 UserDAO userDao = new UserDAO();
		  UserDataBeans user = userDao.findByLoginInfo2(loginId, password);

		  if(user==null) {
			  request.setAttribute("errMsg", "ログインに失敗しました。");

			  RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/um/Login.jsp");
			  dispatcher.forward(request, response);
			  return;
		  }

		  session.setAttribute("userInfo", user);

		  response.sendRedirect("UserListServlet");

	 } catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	 }
}

