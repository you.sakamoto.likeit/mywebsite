package UserManagement;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDAO;

/**
 * Servlet implementation class MCreate
 */
@WebServlet("/MCreate")
public class MCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/um/mCreate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String sName = request.getParameter("sName");
			String sDetail = request.getParameter("sDetail");
			String sMoney = request.getParameter("sMoney");
			String image = request.getParameter("image");
			String sCategory = request.getParameter("sCategory");

			if(sName.equals("")||sDetail.equals("")||sMoney.equals("")||image.equals("")||sCategory.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				  RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/um/mCreate.jsp");
				  dispatcher.forward(request, response);
				  return;
			}


			 ItemDAO itemDao = new ItemDAO();
			 itemDao.InsertIntoCreateInfo(sName,sDetail, sMoney,image,sCategory);

			  response.sendRedirect("MList");
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	 }
	}
