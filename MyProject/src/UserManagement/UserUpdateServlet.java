package UserManagement;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String id = request.getParameter("id");
		int idi = Integer.parseInt(id);

		UserDAO userDao = new UserDAO();
		UserDataBeans user = userDao.findBySentakuInfo(idi);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/um/UserUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		int idi = Integer.parseInt(id);

		UserDAO userDao = new UserDAO();
		UserDataBeans user = userDao.findBySentakuInfo(idi);

		request.setAttribute("user", user);

		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName = request.getParameter("userName");
		String address = request.getParameter("address");
		String loginId = request.getParameter("loginId");
		String updateDate = request.getParameter("update_date");

		UserDataBeans udb = new UserDataBeans();
		udb.setName(userName);
		udb.setAddress(address);
		udb.setLoginId(loginId);
		udb.setPassword(password);


		if(password.equals("")&&passwordConf.equals("")){
			UserDAO userDao3 = new UserDAO();
			userDao3.findByUpdateInfo2(idi,userName,address,updateDate);

			response.sendRedirect("UserListServlet");

			return;

		}else if(!password.equals(passwordConf)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			  dispatcher.forward(request, response);
			  return;

		}else if(userName.equals("")||address.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			  RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			  dispatcher.forward(request, response);
			  return;
		}

		UserDAO userDao2 = new UserDAO();
		userDao2.findByUpdateInfo(idi,password,userName,address,updateDate);

		  response.sendRedirect("UserListServlet");
	}

}
