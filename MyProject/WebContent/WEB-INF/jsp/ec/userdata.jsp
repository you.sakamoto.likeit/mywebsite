<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>ユーザー情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

<body>
<br> <br>
	<div class="container">
		<div class="text-center mb-4">
			<h3>ユーザー情報</h3>
		</div>
	</div>
<br> <br>
<div class="container">
  <div class="album py-5 bg-light">
  <form action="UserDataUpdateConfirm" method="POST">
	<div class="form-row">
		<div class="col">
			<label for="name">名前</label>
			<input type="text"  name="user_name" value="${udb.name}" class="form-control" placeholder="${udb.name}">
		</div>
		<div class="col">
			<label for="name">ログインID</label>
			<input type="text" name="login_id" value="${udb.loginId}" class="form-control" placeholder="${udb.loginId}">
		</div>
	</div>
		<div class="form-group">
			<label for="name">住所</label>
			<input type="text"  name="user_address" value="${udb.address}" class="form-control" placeholder="${udb.address}">
		</div>
		<br>
	  	<div class="button_wrapper">
   			<button class="btn btn btn-outline-success" type="submit" name="action">　　　更新　　　</button>
   		</div>
   	</form>
  </div>

			<br>
	<div class="album py-5 bg-light">
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th class="center" style="width:500px;text-align:center;">購入日時</th>
					<th class="center" style="text-align:center;">配送方法</th>
					<th class="center" style="text-align:center;">合計金額</th>
				</tr>
			</thead>
			<tbody>
		<c:forEach var="buy" items="${buyList}" >
				<tr>
					<td class="center">
						<a href="UserBuyHistoryDetail?id=${buy.id}" class="btn-floating btn waves-effect waves-light">
							<i class="material-icons green">details</i>
						</a>
					</td>
					<td style="text-align:center;">${buy.formatDate}</td>
					<td style="text-align:center;">${buy.deliveryMethodName}</td>
					<td style="text-align:center;">${buy.formatTotalPrice}円</td>
				</tr>
		</c:forEach>
		    </tbody>
		</table>
		</div>
	</div>
</body>
</html>