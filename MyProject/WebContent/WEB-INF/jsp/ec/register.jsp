<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<title>ユーザー登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
  </head>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->
  <body>
<form action="RegisterConfirm" method="POST">
  <div class="container">
<br>

<div class="album py-5 bg-light">
  <div class="text-center mb-4">
    <h1 class="h3 mb-3 font-weight-normal">新規登録</h1>
    		<c:if test="${validationMessage != null}">
				<P class="red-text">${validationMessage}</P>
			</c:if>
  </div>
<br>
  <div class="form-label-group row form-group-style">
  	<label class="col-3">名前</label>
    <input type="text" name="user_name" value="${udb.name}" id="inputName" class="form-control col-7" placeholder="名前" required autofocus>
  </div>
<br>
  <div class="form-label-group row form-group-style">
  <label class="col-3">住所</label>
    <input type="text" value="${udb.address}" name="user_address" id="inputAddress" class="form-control col-7" placeholder="住所" required autofocus>
  </div>
<br>
    <div class="form-label-group row form-group-style">
    <label class="col-3">ログインID</label>
    <input type="text" value="${udb.loginId}" name="login_id" id="inputLoginId" class="form-control col-7" placeholder="ログインID" required autofocus>
  </div>
<br>
  <div class="form-label-group row form-group-style">
  <label class="col-3">パスワード</label>
    <input type="password" name="password" id="inputPassword" class="form-control col-7" placeholder="パスワード" required autofocus>
  </div>
<br>
  <div class="form-label-group row form-group-style">
  <label class="col-3">確認</label>
    <input type="password" name="confirm_password" id="inputPasswordConf" class="form-control col-7" placeholder="パスワード(確認用)" required autofocus>
    <label for="inputPasswordConf"></label>
  </div>
<br>
  <div align="center">
  	<button class="btn btn-outline-success" type="submit" role="button" name="action">　確認　</button>
  </div>
</div>
</div>
</form>

</body>
</html>