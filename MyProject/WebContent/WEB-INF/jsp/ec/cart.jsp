<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>買い物かご</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

<div class="container">
	<div class="row center">
			${cartActionMessage}
	</div>
	<div class="text-center mb-4">
		<h3>買い物かご</h3>
	</div>
<form action="ItemDelete" method="POST">
		<div class="row">
			<div class="col-6"align="center">
				<button class="btn btn btn-outline-primary"
					type="submit" name="action">
					　削除　
				</button>
			</div>
			<div class="col-6"align="center">
				<a href="Buy" class="btn btn btn-outline-success">
					レジに進む
				</a>
			</div>
		</div>
<br>
  <div class="album py-5 bg-light">
    <div class="container">


      <div class="row">
      <c:forEach var="item" items="${cart}" varStatus="status">
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <a href="Item?item_id=${item.id}"><img src="image/${item.fileName}"
            width="338px" height="200px" border="4px" alt="saboten1"></a>
            <div class="card-body">
              <p class="card-text">${item.name}</p>
              <p class="card-price">${item.formatPrice}円</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="custom-control custom-switch">
				  <input type="checkbox" class="custom-control-input" id="${status.index}" name="delete_item_id_list" value="${item.id}">
				  <label class="custom-control-label" for="${status.index}">削除</label>
				</div>
              </div>
            </div>
          </div>
        </div>
        </c:forEach>
	   </div>
	   </form>
	  </div>
<!-- footer -->
<blockquote class="blockquote text-right">
  <footer class="blockquote-footer">Made by <cite title="Source Title">坂本 陽</cite></footer>
</blockquote>
<!-- /footer -->
</body>
</html>