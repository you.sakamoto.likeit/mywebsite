<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>購入完了</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->
</head>
<body>
	<div class="container">
		<br> <br>
		<div class="text-center mb-4">
			<h3>購入完了</h3>
		</div>
<div class="row">
			<div class="col-6 " style="text-align:center;">
				<a href="Index" class="btn btn-outline-success">買い物を続ける</a>
			</div>
			<div class="col-6" style="text-align:center;">
				<a href="UserData" class="btn btn-outline-success">ユーザー情報へ</a>
			</div>
		</div>
		<br>
		<div class="text-center mb-4">
			<h3>購入詳細</h3>
		</div>
	</div>
		<br>
	<div class="album py-5 bg-light col-6" style="margin-left:auto; margin-right:auto">
		<table class="table">
			<thead>
				<tr>
					<th class="center" style="width:500px;text-align:center;">購入日時</th>
					<th class="center" style="text-align:center;">配送方法</th>
					<th class="center" style="text-align:center;">合計金額</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align:center;">${resultBDB.formatDate}</td>
					<td style="text-align:center;">${resultBDB.deliveryMethodName}</td>
					<td style="text-align:center;">${resultBDB.formatTotalPrice}円</td>
				</tr>
		    </tbody>
		</table>
	</div>
<br>
<br>
	<div class="album py-5 bg-light col-6" style="margin-left:auto; margin-right:auto">
		<table class="table">
			<thead>
				<tr>
					<th class="center" style="width:500px;text-align:center;">商品名</th>
					<th class="center" style="text-align:center;">単価</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="buyIDB" items="${buyIDBList}" >
				<tr>
					<td style="text-align:center;">${buyIDB.name}</td>
					<td style="text-align:center;">${buyIDB.formatPrice}円</td>
				</tr>
			</c:forEach>
				<tr>
					<td style="text-align:center;">${resultBDB.deliveryMethodName}</td>
					<td style="text-align:center;">${resultBDB.deliveryMethodPrice}円</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- footer -->
<blockquote class="blockquote text-right">
  <footer class="blockquote-footer">Made by 坂本 陽</cite></footer>
</blockquote>
	<!-- /footer -->
</body>
</html>