<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>システムエラー</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->
</head>
<body>
<br>
<br>
 <body>
<main role="main" class="container">
<div class="text-center mb-4">
  <div class="jumbotron">
    <h1>システムエラーが発生しました</h1>
    <p class="lead">${errorMessage}</p>
    <a class="btn btn-success" href="Index" role="button">TOPに戻る</a>
  </div>
 </div>
</main>
</body>
</html>