<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">

<title>ログイン画面</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
	<body>
		<div class="padding">

			<!-- body -->
<div>
			<form action="Login" method="POST">
				<div class="row">
					<c:if test="${loginErrorMessage != null}">
						<p class="red-text center-align">${loginErrorMessage}</p>
						<br>
					</c:if>
				</div>
				<p style="margin: 150px;"></p>
				<div class="text-center mb-4">
					<h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1>
				</div>
				<br>
					<div class="form-label-group row form-group-style">
						<label class="col-3">ログインID</label> <input type="text"
							name="loginId" value="${inputLoginId}" id="inputName"
							class="form-control col-7" placeholder="LoginID" required autofocus>
					</div>
					<br>

				<div class="form-label-group row form-group-style">
					<label class="col-3">パスワード</label> <input type="password"
						name="password" id="inputPassword" class="form-control col-7"
						placeholder="password" required autofocus>
				</div>
				<br> <br>
				<button class="btn btn-lg btn-success" name="action" type="submit">ログイン</button>
			</form>
		</div>
<br>
<br>
   		<div class = col-12>
			<a class="btn btn-outline" href="Register">新規登録</a>
		</div>
		<style type="text/css">
body {
background-color: #fffdf6;
color: #000000;
width:100%;
		text-align:center;
}
		#wrap{
		width:960px;
		margin:0 auto;
		text-align:left;
		}

		</style>
		</div>
	</body>
</html>