<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Saboten</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />

</head>
<body>

	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

	<p style="margin: 50px;"></p>

	<div class="container">
		<div class="row">

			<div id="side" class="col-sm-2">
				<div class="p-3">
					<h4 class="font-italic">カテゴリ</h4>
					<c:forEach var="item3" items="${itemList3}" >
					<ol class="list-unstyled mb-0">
						<li><a href="Tag?id=${item3.id}"><font color=#488484>${item3.tag}</font></a></li>
					</ol>
					</c:forEach>
				</div>
			</div>
				<div class="col-sm-10">
					<div class="row">
						<div class="col-md-8 blog-main">
							<h3 class="pb-3 mb-4 font-italic border-bottom">検索結果</h3><p>検索結果${itemCount}件</p>
						</div>
					</div>
			<div class="row">
				<c:forEach var="item2" items="${itemList2}">
				<div class="col-sm-4">
					<div class="card mb-4 shadow-sm">
						<div class="card-image">
							<a href="Item?item_id=${item2.id}"><img src="image/${item2.fileName}"width="285px" height="150px" border="4px"></a>
						</div>
						<div class="card-content">
							<p class="card-title" align="center">${item2.name}</p>
							<p align="center">${item2.formatPrice}円
							</p>
						</div>
					</div>
				</div>
				<c:if test="${(status.index + 1) % 4 == 0}">
			</div>
			<div class="row">
				</c:if>
				</c:forEach>
			</div>　

			<div class="row" align="center">
			<ul class="pagination">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a href="ItemSearchResult?search_word=${searchWord}&page_num=${pageNum - 1}"><i class="material-icons">chevron_left</i></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a href="ItemSearchResult?search_word=${searchWord}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a href="SearchResult?search_word=${searchWord}&page_num=${pageNum + 1}"><i class="material-icons">chevron_right</i></a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>
</div>
</div>

	<!-- footer -->
<blockquote class="blockquote text-right">
  <footer class="blockquote-footer">Made by <cite title="Source Title">坂本 陽</cite></footer>
</blockquote>
	<!-- /footer -->
</body>
</html>