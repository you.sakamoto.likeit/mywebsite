<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<title>ユーザー登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
  </head>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

<body>
  <div class="container">
<br>
<div class="container">
<div class="album py-5 bg-light">
<form action="RegisterResult" method="POST">
  <div class="text-center mb-4">
    <img class="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
    <h1 class="h3 mb-3 font-weight-normal">新規登録</h1>
  </div>
<br>
  <div class="form-label-group row form-group-style">
  	<label class="col-3">名前</label>
    <input type="text" name="user_name" value="${udb.name}" id="inputName" class="form-control col-7" readonly>
  </div>
<br>
  <div class="form-label-group row form-group-style">
  	<label class="col-3">住所</label>
    <input type="text" name="user_address" value="${udb.address}" id="inputAddress" class="form-control col-7" readonly>
  </div>
<br>
  <div class="form-label-group row form-group-style">
    <label class="col-3">ログインID</label>
    <input type="text" name="login_id" value="${udb.loginId}" id="inputLoginId" class="form-control col-7" readonly>
  </div>
<br>
  <div class="form-label-group row form-group-style">
  	<label class="col-3">パスワード</label>
    <input type="password" name="password" value="${udb.password}" id="inputPassword" class="form-control col-7" value="saboten2" readonly>
    <label for="inputPassword"></label>
  </div>
<br>
		<div class="row">
			<div class="col s12" style="text-align:center;">
				<p class="center-align">上記内容で登録してよろしいでしょうか?</p>
			</div>
		</div>
<br>
		<div class="row" align="center">
			<div class="col s6 center-align">
				<button class="btn btn-outline-primary" type="submit" name="confirm_button" value="cancel">　修正　</button>
			</div>
			<div class="col s6 center-align">
				<button class="btn btn-outline-success" type="submit" name="confirm_button" value="regist">　登録　</button>
			</div>
		</div>
</form>
	</div>
</div>
	</form>
</div>

</body>
</html>