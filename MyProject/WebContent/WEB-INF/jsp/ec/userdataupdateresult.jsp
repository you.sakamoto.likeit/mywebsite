<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>ユーザー情報更新確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

<body>
<br> <br>
	<div class="container">
		<div class="text-center mb-4">
			<h3>入力内容確認</h3>
		</div>
	</div>
<br>
<form>
<div class="container">
  <div class="album py-5 bg-light">
	<div class="form-row">
		<div class="col">
			<label for="name">名前</label>
			<input type="text" class="form-control" name="user_name_update" value="${udb.name}" readonly>
		</div>
		<div class="col">
			<label for="name">ログインID</label>
			<input type="text" class="form-control" name="login_id_update" value="${udb.loginId}" readonly>
		</div>
	</div>
		<div class="form-group">
			<label for="name">住所</label>
			<input type="text" class="form-control" name="user_address_update" value="${udb.address}" readonly>
		</div>
		<br>
	  		<div class="button_wrapper">
   			<h5 align="center">上記内容で更新しました</h5>
   			</div>
		<br>
	  	<div class="button_wrapper">
   			<a class="btn btn-outline-success" href="UserData" role="button">ユーザー情報へ</a>
   		</div>
  </div>
</body>
</html>