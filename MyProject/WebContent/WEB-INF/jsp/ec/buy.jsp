<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
<!-- /header -->
<div class="container">
	<br>
	<br>
	<div align="center">
		<h3>カートアイテム</h3>
	</div>
	<br>

<form action="BuyConfirm" method="POST">
<div class="album py-5 bg-light">
		<table class="table">
  <thead>
    <tr>
      <th class="center" style="width: 40%;text-align:center;">商品名</th>
      <th class="center" style="text-align:center;">単価</th>
      <th class="center" style="width: 20%;text-align:center;">小計</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="cartInItem" items="${cart}" >
    <tr>
      	<td align="center">${cartInItem.name}</td>
		<td align="center">${cartInItem.formatPrice}円</td>
		<td align="center">${cartInItem.formatPrice}円</td>
    </tr>
  </c:forEach>
    <tr>
      <th scope="row"></th>
					<td class="center"></td>
					<td class="center">
				配送方法<div class="input-field col s8 offset-s2">
							<select name="delivery_method_id">
							<c:forEach var="dmdb" items="${dmdbList}" >
								<option value="${dmdb.id}">${dmdb.name}</option>
							</c:forEach>
							</select>
						</div>
					</td>
				</tr>
  </tbody>
</table>
			<div class="row">
				<div class="col s12" align="center">
					<button class="btn  btn-outline-success" type="submit" name="action">購入確認</button>
				</div>
			</div>
</div>
</form>
</div>
<!-- footer -->
<blockquote class="blockquote text-right">
  <footer class="blockquote-footer">Made by <cite title="Source Title">坂本 陽</cite></footer>
</blockquote>
<!-- /footer -->
</body>
</html>