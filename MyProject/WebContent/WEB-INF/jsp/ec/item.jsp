<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Saboten</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />

</head>


<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

	<div class="container">
	<br>
		<div class="row center">
				<div class="col s8">
					<h4 class=" col s12 light">商品詳細</h4>
				</div>
				<div class="col s4">
					<form action="ItemAdd" method="POST">
						<input type="hidden" name="item_id" value="${item.id}">
						<button class="btn btn btn-outline-success">
							買い物かごに追加
						</button>
					</form>
				</div>
			</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col s6">
					<div class="card">
						<div class="card-image">
							<img src="image/${item.fileName}"width="980px" height="500px" border="9px">
						</div>
					</div>
				</div>
				<div class="col s6">
					<h4>${item.name}</h4>
					<h5>${item.formatPrice}円</h5>
					<p>${item.detail}
				</div>
			</div>
			<!-- footer -->
<blockquote class="blockquote text-right">
  <footer class="blockquote-footer">Made by <cite title="Source Title">坂本 陽</cite></footer>
</blockquote>
			<!-- /footer -->
</body>
</html>