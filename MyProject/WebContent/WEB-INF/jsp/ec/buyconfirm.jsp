<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>購入確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

<body>
	<div class="container">
		<br>
		<br>
		<div class="text-center mb-4">
			<h3>購入</h3>
		</div>
	</div>
	<br>
	<div class="album py-5 bg-light col-6" style="margin-left:auto; margin-right:auto">
		<table class="table">
			<thead>
				<tr>
					<th class="center" style="width:500px;text-align:center;">商品名</th>
					<th class="center" style="text-align:center;">単価</th>
					<th class="center" style="text-align:center;">小計</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="cartInItem" items="${cart}" >
				<tr>
					<td style="text-align:center;">${cartInItem.name}</td>
					<td style="text-align:center;">${cartInItem.formatPrice}円</td>
					<td style="text-align:center;">${cartInItem.formatPrice}円</td>
				</tr>
			</c:forEach>
				<tr>
					<td style="text-align:center;">${bdb.deliveryMethodName}</td>
					<td style="text-align:center;">${bdb.deliveryMethodPrice}円</td>
					<td style="text-align:center;">${bdb.deliveryMethodPrice}円</td>
				</tr>
				<tr>
					<td></td>
					<td style="text-align:center;">合計</td>
					<td style="text-align:center;">${bdb.formatTotalPrice}円</td>
				</tr>
			</tbody>
		</table>
		<div class="row">
		<div class="col s12"align="center">
			<form action="BuyResult" method="post">
				<button class="btn  btn-outline-success" type="submit">　購入　</button>
			</form>
		</div>
	</div>
</div>
<!-- footer -->
<blockquote class="blockquote text-right">
  <footer class="blockquote-footer">Made by <cite title="Source Title">坂本 陽</cite></footer>
</blockquote>
<!-- /footer -->
</body>
</html>