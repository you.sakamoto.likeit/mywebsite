<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Saboten</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />

</head>


<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

	<!-- container -->
	<p style="margin: 50px;"></p>

	<div class="container">
		<div class="row">

			<div id="side" class="col-sm-2">
				<div class="p-3">
					<h4 class="font-italic">カテゴリ</h4>
					<c:forEach var="item3" items="${itemList3}" >
					<ol class="list-unstyled mb-0">
						<li><a href="Tag?id=${item3.id}"><font color=#488484>${item3.tag}</font></a></li>
					</ol>
					</c:forEach>
				</div>
			</div>

			<div class="col-sm-10">
				<div>
					<div class="row">
						<div class="col-md-8 blog-main">
							<h3 class="pb-3 mb-4 font-italic border-bottom">サボテン専門通販サイト</h3>
						</div>
					</div>

				<div class="row">
				<c:forEach var="item2" items="${itemList2}">
				<div class="col-sm-4">
					<div class="card mb-4 shadow-sm">
						<div class="card-image">
							<a href="Item?item_id=${item2.id}"><img src="image/${item2.fileName}"width="285px" height="150px" border="4px"></a>
						</div>
						<div class="card-content">
							<p class="card-title size_test" align="center">${item2.name}</p>
							<p align="center">${item2.formatPrice}円</p>
						</div>
					</div>
				</div>
				</c:forEach>
			</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer -->
<blockquote class="blockquote text-right">
  <footer class="blockquote-footer">Made by 坂本 陽</footer>
</blockquote>
 	<!-- /footer -->
</body>
</html>