<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>ユーザー購入履歴</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />
	<!-- /header -->

<body>
<br> <br>
	<div class="container">
		<div class="text-center mb-4">
			<h3>ユーザー情報</h3>
		</div>
	</div>
<br> <br>
	<div class="album py-5 bg-light col-6" style="margin-left:auto; margin-right:auto">
		<table class="table">
			<thead>
				<tr>
					<th class="center" style="width:500px;text-align:center;">購入日時</th>
					<th class="center" style="text-align:center;">配送方法</th>
					<th class="center" style="text-align:center;">合計金額</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align:center;">${buy.formatDate}</td>
					<td style="text-align:center;">${buy.deliveryMethodName}</td>
					<td style="text-align:center;">${buy.formatTotalPrice}円</td>
				</tr>
								<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
		    </tbody>
		</table>
	</div>
<br>
<br><br>
	<div class="album py-5 bg-light col-6" style="margin-left:auto; margin-right:auto">
		<table class="table">
			<thead>
				<tr>
					<th class="center" style="width:700px;text-align:center;">商品名</th>
					<th class="center" style="text-align:center;">単価</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="buy3" items="${buyList3}" >
				<tr>
					<td style="text-align:center;">${buy3.name}</td>
					<td style="text-align:center;">${buy3.formatPrice}円</td>
				</tr>
			</c:forEach>
				<tr>
					<td style="text-align:center;">${buy.deliveryMethodName}</td>
					<td style="text-align:center;">${buy.deliveryMethodPrice}円</td>
				</tr>
			</tbody>
		</table>
	</div>
	</div>
</body>
</html>