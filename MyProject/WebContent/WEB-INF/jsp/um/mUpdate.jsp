<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
  <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
    <div class="container">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">

        <li class="nav-item active">
          <a class="nav-link mr-5" href="UserListServlet">ユーザ管理システム</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="MList">商品マスタ一覧</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="navbar-text mr-5" href="UserDetailServlet?id=${userInfo.id}">${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-success" href="LogOutServlet">ログアウト</a>
        </li>
      </ul>
      </div>
    </nav>
  </header>
  <!-- /header -->
<div class="container">

	<form action="MUpdate" method="post">

	<p style="margin:50px;"></p>
	<div align="center">
      <h1>商品情報更新</h1>
    </div>
		<p style="margin:50px;"></p>

		商品ID　　　　　　　　　${item.id}
<input type="hidden" name="id" value="${item.id}">

 <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">商品名</label>
    <div class="col-sm-10">
      <input type="text" name="sName" id="sName" class="form-control" value="${item.name}">
    </div>
  </div>

 <div class="form-group row">
    <label for="passwordConf" class="col-sm-2 col-form-label">金額</label>
    <div class="col-sm-10">
      <input type="text" name="sMoney" id="money" class="form-control" value="${item.price}">
    </div>
  </div>

	<div class="form-group row">
    <label for="passwordConf" class="col-sm-2 col-form-label">画像</label>
    <div class="col-sm-10">
      <input type="text" name="fileName" id="image" class="form-control" value="${item.fileName}">
    </div>
  </div>

		      <div class="form-group row">
		        <label for="birthDate" class="col-sm-2 col-form-label">詳細</label>
		        <div class="col-sm-10">
		          <input type="text" name="sDetail" id="detail" class="form-control" value="${item.detail}">
		        </div>
		      </div>

      		<div class="form-group row">
				<label for="detail" class="col-sm-2 col-form-label">タグ番号</label>
				<div class="col-sm-10">
					<input type="text" name="sCategory" value="${item.tagId}" id="sDetail" class="form-control">
				</div>
			</div>
<br>
<br>
<br>
		<div class="row">
			<div class="col-6">
				<button class="btn btn-outline-success col s6 offset-s3">
				更新
				</button>
			</div>
			<div class="col-6">
				<a href="MList" class="btn btn-outline-primary col s6 offset-s3">
				戻る
				</a>
			</div>
		</div>
	</form>
</div>
</body>
</html>