<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
    <div class="container">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">

        <li class="nav-item active">
          <a class="nav-link mr-5" href="UserListServlet">ユーザ管理システム</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="MList">商品マスタ一覧</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="navbar-text mr-5" href="UserDetailServlet?id=${userInfo.id}">${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-success" href="LogOutServlet">ログアウト</a>
        </li>
      </ul>
      </div>
    </nav>
  </header>
  <!-- /header -->

	<body>
	<div class="container">

	<!-- 新規登録ボタン -->
	<p style="margin:50px;"></p>
   		<div class = text-right>
			<a class="btn btn-outline-success" href="UserCreate">ユーザー新規登録</a>
		</div>

	<!-- body -->
  <div class="container">

	<p style="margin:100px;"></p>
	<div class="row">
        <div class="col-sm-5"></div>
	<h1>ユーザー一覧</h1>
		</div>
<br>
<br>

<!-- 新規登録ボタン -->

<form class="form-userlist" action="UserListServlet" method="post">
	 <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <input type="text" id="loginId" class="form-control"name="loginId">
    </div>
  </div>

 <div class="form-group row">
   <label for="loginId" class="col-sm-2 col-form-label">ユーザー名</label>
     <div class="col-sm-10">
      <input type="text" id="loginId" class="form-control"name="userName">
     </div>
  </div>

  <div class="form-group row">
   <label for="loginId" class="col-sm-2 col-form-label">住所</label>
     <div class="col-sm-10">
      <input type="text" id="address" class="form-control"name="address">
     </div>
  </div>


	<div class = text-right>
		<button type="submit" class="btn btn-outline-success">　検索　</button>
	</div>
</form>

	<hr size="5" color="green" noshade>

 <!-- 検索結果一覧 -->
         <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th style="text-align:center;">ログインID</th>
                   <th style="text-align:center;">ユーザ名</th>
                   <th style="text-align:center;">住所</th>
                   <th style="text-align:center;"></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td style="text-align:center;">${user.loginId}</td>
                     <td style="text-align:center;">${user.name}</td>
                     <td style="text-align:center;">${user.address}</td>
                     <td style="text-align:center;">

					<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                    <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                    <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>

                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
         </div>
      </div>

</body>
</body>
</html>