<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品情報詳細参照</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
  <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
    <div class="container">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">

        <li class="nav-item active">
          <a class="nav-link mr-5" href="UserListServlet">ユーザ管理システム</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="MList">商品マスタ一覧</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="navbar-text mr-5" href="UserDetailServlet?id=${userInfo.id}">${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-success" href="LogOutServlet">ログアウト</a>
        </li>
      </ul>
      </div>
    </nav>
  </header>
  <!-- /header -->
  <div class="container">

	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-5"></div>
	<h1>商品情報</h1>
		</div>
		<p style="margin:50px;"></p>


      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">商品ID</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${item.id}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">商品名</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${item.name}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="money" class="col-sm-2 col-form-label">金額</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${item.formatPrice}円</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="updateDate" class="col-sm-2 col-form-label">画像</label>
        <div class="col-sm-10">
          <img src="image/${item.fileName}"
							width="200px" height="150px" border="4px" alt="saboten1">
        </div>
      </div>

      <div class="form-group row">
        <label for="text" class="col-sm-2 col-form-label">詳細</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${item.detail}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="text" class="col-sm-2 col-form-label">タグ番号</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${item.tagId}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="text" class="col-sm-2 col-form-label">タグ名</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${item.tag}</p>
        </div>
      </div>

      <div class="col-xs-4">
        <a href="MList" class="btn btn-outline-primary btn-lg btn-block">戻る</a>
      </div>
  </div>
</body>
</html>