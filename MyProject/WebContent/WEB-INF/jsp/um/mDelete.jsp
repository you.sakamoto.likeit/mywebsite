<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品削除確認</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
    <div class="container">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">

        <li class="nav-item active">
          <a class="nav-link mr-5" href="UserListServlet">ユーザ管理システム</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="MList">商品マスタ一覧</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="navbar-text mr-5" href="UserDetailServlet?id=${userInfo.id}">${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-success" href="LogOutServlet">ログアウト</a>
        </li>
      </ul>
      </div>
    </nav>
  </header>
  <!-- /header -->

		  <!-- body -->
  <div class="container">

	<p style="margin:50px;"></p>
	<div align="center">
      <h1>商品削除確認</h1>
    </div>
		<p style="margin:50px;"></p>

 <div class="container">
 <div class="delete-area">
 	<div align="center">
      <p>${item.name}を消去しますか？</p>
    </div>
      <br>
      <br>
      <div class="row">
        <div class="col-sm-6">
          <a href="MList" class="btn btn-outline-primary btn-block">いいえ</a>
        </div>
        <div class="col-sm-6">
        <form action="MDelete" method="post">
          <button class="btn btn-outline-success btn-block"name="id"value="${item.id}">はい</button>
        </form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>