<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
  <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
    <div class="container">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">

        <li class="nav-item active">
          <a class="nav-link mr-5" href="UserListServlet">ユーザ管理システム</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="MList">商品マスタ一覧</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="navbar-text mr-5" href="UserDetailServlet?id=${userInfo.id}">${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-success" href="LogOutServlet">ログアウト</a>
        </li>
      </ul>
      </div>
    </nav>
  </header>
  <!-- /header -->

		  <!-- body -->
  <div class="container">

	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-4"></div>
	<h1>ユーザー新規登録</h1>
		</div>
		<p style="margin:50px;"></p>

<form class="form-create" action=UserCreate method="post">
  	<div class="form-group row">
    		<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    	<div class="col-sm-10">
      		<input type="text" id="text3a" class="form-control" name="loginId">
    	</div>
   	</div>

    <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <input type="text" id="userName" class="form-control" name="userName">
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">住所</label>
        <div class="col-sm-10">
          <input type="text" id="address" class="form-control" name="address">
        </div>
      </div>

  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
      <input type="password" id="password" class="form-control" name="password">
    </div>
  </div>

 <div class="form-group row">
    <label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
      <input type="password" id="passwordConf" class="form-control" name="passwordConf">
    </div>
  </div>



<br>
<br>
<br>
		<div class="row">
			<div class="col-6">
				<button type="submit" class="btn  btn btn-outline-success col s6 offset-s3">
				登録
				</button>
			</div>
			<div class="col-6">
				<a href="UserListServlet" class="btn  btn btn-outline-primary col s6 offset-s3">
				戻る
				</a>
			</div>
		</div>
</form>
</body>
</html>