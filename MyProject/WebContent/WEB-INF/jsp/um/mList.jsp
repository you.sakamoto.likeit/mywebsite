<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品マスタ一覧</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
  <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
    <div class="container">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">

        <li class="nav-item active">
          <a class="nav-link mr-5" href="UserListServlet">ユーザ管理システム</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="MList">商品マスタ一覧</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="navbar-text mr-5" href="UserDetailServlet?id=${userInfo.id}">${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-success" href="LogOutServlet">ログアウト</a>
        </li>
      </ul>
      </div>
    </nav>
  </header>
  <!-- /header -->
	<body>
	<div class="container">

	<!-- 新規登録ボタン -->
	<p style="margin:50px;"></p>
        	<div class = text-right>
				<a class="btn btn-outline-success" href="MCreate">商品新規登録</a>
		   	</div>
		   	<div class = text-right>
				<a class="btn btn-outline-success" href="MkCreate">カテゴリ新規登録</a>
		   	</div>
	</div>

	<!-- body -->
  <div class="container">

	<p style="margin:100px;"></p>
	<div class="row">
        <div class="col-sm-5"></div>
	<h1>商品マスタ一覧</h1>
		</div>

<!-- 新規登録ボタン -->

<form action="MList" method="post">
	 <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">商品ID</label>
    <div class="col-sm-10">
      <input type="text" id="loginId" class="form-control" name="sId">
    </div>
  </div>
 <div class="form-group row">
   <label for="loginId" class="col-sm-2 col-form-label">商品名</label>
     <div class="col-sm-10">
      <input type="text" id="loginId" class="form-control" name="sName">
     </div>
  </div>

<div class="form-group row">
		<label for="birthDate" class="col-sm-2 col-form-label">金額</label>
  	<div class="row col-sm-10">
  		<div class="col-sm-3">
   	 		<input type="text" class="money form-control" id="date-start" name="saki" placeholder="円">
  		</div>

 	<div class="col-sm-2 mt-2">～</div>
 	 	<div class="col-sm-6">
   	 			<input type="text" class="money form-control" id="date-end" name="ato" placeholder="円">
  		</div>
	</div>
</div>

	<div class = text-right>
		<button type="submit" class="btn btn-outline-success">検索</button>
</form>
	<hr size="5" color="green" noshade>

 <!-- 検索結果一覧 -->
    <div class="table-responsive">
      <table class="table table-striped">
        <thead class="thead-dark">
          <tr>
            <th style="text-align:center;">商品ID</th>
            <th style="text-align:center;">商品名</th>
            <th style="text-align:center;">金額</th>
            <th style="text-align:center;">画像</th>
            <th style="text-align:center;"></th>
          </tr>
        </thead>
        <tbody>
        <c:forEach var="item" items="${itemList}" >
          <tr>
            <td style="text-align:center;">${item.id}</td>
            <td style="text-align:center;">${item.name}</td>
            <td style="text-align:center;">${item.formatPrice}円</td>
            <td style="text-align:center;">
            <img src="image/${item.fileName}"
					width="39px" height="39px" border="4px"></td>
            <td>
            <div  class="button_wrapper">
              <a class="btn btn-primary" href="MDetail?id=${item.id}">詳細</a>
              <a class="btn btn-success" href="MUpdate?id=${item.id}">更新</a>
              <a class="btn btn-danger" href="MDelete?id=${item.id}">削除</a>
            </div>
            </td>
          </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>

  </div>
  </div>

</body>
</html>

</html>