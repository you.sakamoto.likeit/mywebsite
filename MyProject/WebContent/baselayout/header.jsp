<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<head>
<link href="/WEB-INF/css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<!-- header -->
<nav class="navbar navbar-light bg-light navbar-expand  flex-md-row">
	<div class="nav-wrapper container">
		<a class="nav-link" href="Index"><h3><font color=#2e8b57>Saboten</font></h3></a></li>　　　　
		<div class="navbar-nav navbar-light bg-light flex-row mr-auto head">
			<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>
			<form action="SearchResult" class="form-inline">
				<div class="form-group mb-9">
					<input type="text" name="search_word" class="form-control" placeholder="Search">
			    	<button type="submit" class="btn btn-outline material-icons green md-36 mb-3 mt-3">search</button>
				</div>
			</form>
	</div>
		<ul class="navbar-nav navbar-light bg-light flex-row mr">
			<%if(isLogin){ %>
			<li><a href="UserData"><i class="material-icons green md-36">account_circle</i></a></li>　　
			<%}else{ %>
			<li><a href="Register"><i class="material-icons green md-36">add</i></a></li>　　
			<%} %>

			<li><a href="Cart"><i class="material-icons green md-36">shopping_cart</i></a></li>　　

			<%if(isLogin){ %>
			<li><a href="Logout"><i class="material-icons green md-36">exit_to_app</i></a></li>　　
			<%}else{ %>
			<li><a href="Login"><i class="material-icons green md-36">vpn_key</i></a></li>　　
			<%} %>
		</ul>
	</div>
</nav>